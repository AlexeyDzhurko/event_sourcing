<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => uniqid(),
                'email' => 'test@test.com',
                'name' => 'admin',
                'password' => Hash::make('testpass'),
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
