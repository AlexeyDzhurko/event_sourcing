include .env

all: | ${APP_ENV}
local: | docker-build docker-up composer-install npm-install npm-run-dev
stage: | docker-build docker-up composer-install npm-install npm-run-dev
prod: | docker-build docker-up composer-install npm-install npm-run-dev

##### composer #####

composer-install:
	docker-compose exec php-fpm composer install

##### npm #####

npm-install:
	docker-compose exec node npm i

npm-run-dev:
	docker-compose exec node npm run dev

##### docker compose #####
docker-build:
	docker-compose build

docker-up:
	docker-compose up -d

docker-rebuild:
	docker-compose down \
	&& docker-compose up -d --build \
	&& docker-compose exec php-fpm composer install

