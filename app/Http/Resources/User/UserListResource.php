<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class UserListResource
 * @package App\Http\Resources\User
 */
class UserListResource extends ResourceCollection
{
    /**
     * UserListResource constructor.
     * @param $resource
     */
    public function __construct($resource)
    {
        if ($resource instanceof LengthAwarePaginator) {
            $this->collection = $resource->items();
            $this->resource = $resource;
        } else {
            parent::__construct($resource);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];
        $response = [];

        foreach ($this->collection as $item) {
            $data[] = new UserResource($item);
        }

        $response['data'] = $data;

        if ($this->resource instanceof LengthAwarePaginator) {
            $response['meta'] = [
                'current_page' => $this->resource->currentPage(),
                'last_page' => $this->resource->lastPage(),
                'per_page' => $this->resource->perPage(),
                'total' => $this->resource->total()
            ];
        }

        return $response;
    }
}
