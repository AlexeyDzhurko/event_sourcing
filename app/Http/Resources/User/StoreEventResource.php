<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class StoreEventResource
 * @package App\Http\Resources\User
 */
class StoreEventResource extends Resource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'id' => $this->user_id ?? null,
                'type' => strtolower($this->type)
            ]
        ];
    }
}
