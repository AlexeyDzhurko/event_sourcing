<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UserResource
 * @package App\Http\Resources\User
 */
class UserResource extends Resource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'id' => $this->id,
                'type' => 'user',
                'attributes' => [
                    'email' => $this->email,
                    'name' => $this->name,
                    'createdAt' => $this->created_at,
                    'updatedAt' => $this->updated_at,
                ]
            ]
        ];
    }
}
