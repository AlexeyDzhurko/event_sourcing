<?php

namespace App\Http\Requests\Dashboard\User;

use Urameshibr\Requests\FormRequest;

/**
 * Class StoreUserRequest
 * @package App\Http\Requests\Dashboard\User
 */
class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'name' => 'required|string',
            'password' => 'required|string|min:8',
            'passwordConfirmation' => 'required|same:password',
        ];
    }
}
