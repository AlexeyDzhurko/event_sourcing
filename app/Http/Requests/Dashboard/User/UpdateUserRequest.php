<?php

namespace App\Http\Requests\Dashboard\User;

use Urameshibr\Requests\FormRequest;

/**
 * Class UpdateUserRequest
 * @package App\Http\Requests\Dashboard\User
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = collect($this->route())->pluck('id')->last();
        return [
            'email' => 'nullable|email|unique:users,email,' . $id,
            'name' => 'nullable|string',
            'password' => 'nullable|string|min:8',
            'passwordConfirmation' => 'required_with:password|same:password',
        ];
    }
}
