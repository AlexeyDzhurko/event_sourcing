<?php

namespace App\Http\Controllers\Dashboard;

use App\Application\User\GetUserById\GetUserById;
use App\Application\User\GetUsersList\GetUsersList;
use App\Application\User\StoreUserEvent\StoreUserEvent;
use App\Domain\Event\User\UserEvent;
use App\Domain\Storage\User\UserFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\User\StoreUserRequest;
use App\Http\Requests\Dashboard\User\UpdateUserRequest;
use App\Http\Requests\Dashboard\User\UserListRequest;
use App\Http\Resources\User\StoreEventResource;
use App\Http\Resources\User\UserListResource;
use App\Http\Resources\User\UserResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package App\Http\Controllers\Dashboard
 */
class UserController extends Controller
{
    /**
     * @param UserListRequest $request
     * @return JsonResponse
     */
    public function index(UserListRequest $request)
    {
        $list = $this->execute(new GetUsersList(
            UserFilter::fromRequest($request),
            Pagination::fromRequest($request),
            Sorting::fromRequest($request)
        ));

        return new JsonResponse(
            new UserListResource($list),
            Response::HTTP_OK
        );
    }

    /**
     * @param string $id
     * @return JsonResponse
     */
    public function view($id)
    {
        $user = $this->execute(new GetUserById($id));

        return new JsonResponse(
            new UserResource($user),
            Response::HTTP_OK
        );
    }

    /**
     * @param StoreUserRequest $request
     * @return JsonResponse
     */
    public function create(StoreUserRequest $request)
    {
        $event = $this->execute(new StoreUserEvent(
            UserEvent::USER_EVENT_CREATE,
            $request->all()
        ));

        return new JsonResponse(
            new StoreEventResource($event),
            Response::HTTP_ACCEPTED
        );
    }

    /**
     * @param UpdateUserRequest $request
     * @param string $id
     * @param string $field
     * @return JsonResponse
     */
    public function update(UpdateUserRequest $request, $id, $field)
    {
        $event = $this->execute(new StoreUserEvent(
            UserEvent::USER_EVENT_UPDATE,
            [$field => $request->get($field)],
            $id
        ));

        return new JsonResponse(
            new StoreEventResource($event),
            Response::HTTP_ACCEPTED
        );
    }

    /**
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     */
    public function delete(Request $request, $id)
    {
        $event = $this->execute(new StoreUserEvent(
            UserEvent::USER_EVENT_DELETE,
            $request->all(),
            $id
        ));

        return new JsonResponse(
            new StoreEventResource($event),
            Response::HTTP_ACCEPTED
        );
    }
}
