<?php

namespace App\Http\Controllers\Dashboard;

use App\Application\Auth\LoginIntoDashboard\LoginIntoDashboard;
use App\Application\Auth\RefreshToken\RefreshToken;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Auth\LoginRequest;
use App\Http\Resources\Auth\AuthResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class AuthController
 * @package App\Http\Controllers\Dashboard
 */
class AuthController extends Controller
{
    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $tokenData = $this->execute(new LoginIntoDashboard($request->all()));

        return new JsonResponse(new AuthResource($tokenData));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function refreshToken(Request $request)
    {
        $tokenData = $this->execute(new RefreshToken($request->header('Authorization')));

        return new JsonResponse(new AuthResource($tokenData));
    }
}
