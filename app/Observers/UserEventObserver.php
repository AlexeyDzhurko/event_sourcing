<?php

namespace App\Observers;

use App\Domain\Event\User\UserEvent;

/**
 * Class UserEventObserver
 * @package App\Observers
 */
class UserEventObserver
{
    /**
     * Handle the app domain event user user event "created" event.
     *
     * @param UserEvent $userEvent
     * @return void
     */
    public function created(UserEvent $userEvent)
    {
        $chanel = (UserEvent::USER_EVENT_CHANNEL . $userEvent->type);
        fire($chanel, [$userEvent->toArray()]);
    }
}
