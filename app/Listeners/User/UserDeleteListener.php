<?php

namespace App\Listeners\User;

use App\Application\User\DeleteUser\DeleteUser;
use App\Application\User\GetUserById\GetUserById;
use App\Infrastructure\Core\CommandBus;
use Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserDeleteListener
 * @package App\Listeners\User
 */
class UserDeleteListener
{
    /** @var CommandBus $commandBus */
    protected $commandBus;

    /**
     * UserDeleteListener constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * Handle the event.
     *
     * @param array $payload
     * @throws Exception
     */
    public function handle(array $payload)
    {
        $user = $this->commandBus->dispatch(new GetUserById($payload['user_id']));

        if (!$user) {
            throw new NotFoundHttpException();
        }

        $this->commandBus->dispatch(new DeleteUser($user));
    }
}
