<?php

namespace App\Listeners\User;

use App\Application\User\GetUserById\GetUserById;
use App\Application\User\UpdateUser\UpdateUser;
use App\Infrastructure\Core\CommandBus;
use Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserUpdateListener
 * @package App\Listeners\User
 */
class UserUpdateListener
{
    /** @var CommandBus $commandBus */
    protected $commandBus;

    /**
     * UserUpdateListener constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * Handle the event.
     *
     * @param array $payload
     * @throws Exception
     */
    public function handle(array $payload)
    {
        $user = $this->commandBus->dispatch(new GetUserById($payload['user_id']));

        if (!$user) {
            throw new NotFoundHttpException();
        }

        $this->commandBus->dispatch(new UpdateUser($user, $payload['params']));
    }
}
