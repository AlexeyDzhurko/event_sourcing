<?php

namespace App\Listeners\User;

use App\Application\User\StoreUser\StoreUser;
use App\Infrastructure\Core\CommandBus;
use Exception;

/**
 * Class UserCreateListener
 * @package App\Listeners\User
 */
class UserCreateListener
{
    /** @var CommandBus $commandBus */
    protected $commandBus;

    /**
     * UserCreateListener constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * Handle the event.
     *
     * @param array $payload
     * @throws Exception
     */
    public function handle($payload)
    {
        $this->commandBus->dispatch(
            new StoreUser(
                $payload['user_id'],
                $payload['params']
            )
        );
    }
}
