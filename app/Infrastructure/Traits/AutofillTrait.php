<?php

namespace App\Infrastructure\Traits;

/**
 * Trait AutofillTrait
 * @package App\Infrastructure\Traits
 */
trait AutofillTrait
{
    /**
     * @param array $properties
     */
    public function autofillProperties(array $properties)
    {
        foreach ($properties as $key => $property) {
            if (property_exists(self::class, $key)) {
                $this->$key = $property;
            }
        }
    }
}
