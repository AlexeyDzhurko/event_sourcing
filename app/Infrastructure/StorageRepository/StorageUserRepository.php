<?php

namespace App\Infrastructure\StorageRepository;

use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Storage\User\User;
use App\Domain\Storage\User\UserFilter;
use App\Domain\Storage\User\UserRepositoryInterface;
use App\Infrastructure\Errors\StorageModelError;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class UserRepository
 * @package App\Infrastructure\StorageRepository
 */
class StorageUserRepository implements UserRepositoryInterface
{
    /** @var User $model */
    private $model;

    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param UserFilter $filter
     * @param PaginationInterface $paginator
     * @param SortingInterface|null $sorting
     * @return LengthAwarePaginator
     */
    public function allPagination(
        UserFilter $filter,
        ?PaginationInterface $paginator = null,
        ?SortingInterface $sorting = null
    ): LengthAwarePaginator {
        /** @var Builder $builder */
        $builder = $this->model->newQuery();
        $builder = $this->filter($filter, $builder);


        if ($sorting) {
            $builder->orderBy($sorting->getField(), $sorting->getDirection());
        }

        if ($paginator) {
            return $builder->paginate($paginator->getPerPage(), ['*'], 'page', $paginator->getPage());
        }

        return $builder->get();
    }

    /**
     * @param UserFilter $filter
     * @param SortingInterface|null $sorting
     * @return Collection
     */
    public function all(UserFilter $filter, ?SortingInterface $sorting): Collection
    {
        $builder = $this->model->newQuery();
        $builder = $this->filter($filter, $builder);

        return $builder->get();
    }

    /**
     * @param UserFilter $filter
     * @return User|null
     */
    public function one(UserFilter $filter): ?Model
    {
        /** @var Builder $builder */
        $builder = $this->model->newQuery();
        $builder = $this->filter($filter, $builder);

        return $builder->first();
    }

    /**
     * @param string $id
     * @return Model|null
     */
    public function byId(string $id): ?Model
    {
        $builder = $this->model->newQuery();

        return $builder->findOrFail($id);
    }

    /**
     * @param User $model
     * @return User
     * @throws StorageModelError
     */
    public function store(User $model): User
    {
        if (!$model->save()) {
            throw StorageModelError::notSaved(User::class, $model->id);
        }

        return $model;
    }

    /**
     * @param User $model
     * @throws \Exception
     */
    public function delete(User $model): void
    {
        $model->delete();
    }

    /**
     * @param UserFilter $filter
     * @param Builder $builder
     * @return Builder
     */
    private function filter(UserFilter $filter, Builder $builder)
    {
        if ($email = $filter->getEmail()) {
            $builder->where('email', $email);
        }

        return $builder;
    }
}
