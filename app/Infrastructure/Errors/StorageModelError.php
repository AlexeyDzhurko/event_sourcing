<?php

namespace App\Infrastructure\Errors;

use Exception;
use Illuminate\Http\Response;

/**
 * Class StorageModelError
 * @package App\Infrastructure\Errors
 */
class StorageModelError extends Exception
{
    /**
     * @param string $class
     * @param string $id
     * @return StorageModelError
     */
    public static function notSaved(string $class, string $id)
    {
        return new self(
            sprintf('%s #%s not saved', $class, $id),
            Response::HTTP_BAD_REQUEST
        );
    }
}
