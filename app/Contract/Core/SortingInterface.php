<?php

namespace App\Contract\Core;

use App\Infrastructure\Core\Sorting;
use Illuminate\Http\Request;

/**
 * Class SortingInterface
 * @package App\Contract\Core
 */
interface SortingInterface
{
    /**
     * @param Request $request
     * @return Sorting
     */
    public static function fromRequest(Request $request);

    /**
     * @return string
     */
    public function getField(): string;

    /**
     * @return string
     */
    public function getDirection(): string;
}
