<?php

namespace App\Providers;
use App\Contract\Services\JwtServiceInterface;
use App\Infrastructure\Services\JwtService;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Http\Request;

/**
 * Class AuthServiceProvider
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        $this->app['auth']->viaRequest('api', function (Request $request) {
            /** @var JwtService $jwtService */
            $jwtService = $this->app->make(JwtServiceInterface::class);

            return $jwtService->authByToken($request->header('Authorization'));
        });
    }
}
