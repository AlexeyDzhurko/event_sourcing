<?php

namespace App\Providers;

use App\Domain\Event\User\UserEvent;
use App\Listeners\User\UserCreateListener;
use App\Listeners\User\UserDeleteListener;
use App\Listeners\User\UserUpdateListener;
use App\Observers\UserEventObserver;
use Nuwber\Events\BroadcastEventServiceProvider as CoreBroadcastEventServiceProvider;

/**
 * Class BroadcastEventServiceProvider
 * @package App\Providers
 */
class BroadcastEventServiceProvider extends CoreBroadcastEventServiceProvider
{
    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function boot()
    {
        parent::boot();
        UserEvent::observe(UserEventObserver::class);
    }

    /**
     * @var
     */
    protected $listen = [
        (UserEvent::USER_EVENT_CHANNEL . UserEvent::USER_EVENT_CREATE) => [
            UserCreateListener::class
        ],
        (UserEvent::USER_EVENT_CHANNEL . UserEvent::USER_EVENT_UPDATE) => [
            UserUpdateListener::class
        ],
        (UserEvent::USER_EVENT_CHANNEL . UserEvent::USER_EVENT_DELETE) => [
            UserDeleteListener::class
        ],
    ];
}
