<?php

namespace App\Providers;

use App\Domain\Storage\User\UserRepositoryInterface;
use App\Infrastructure\StorageRepository\StorageUserRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class StorageServiceProvider
 * @package App\Providers
 */
class StorageServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(
            UserRepositoryInterface::class,
            StorageUserRepository::class
        );
    }
}
