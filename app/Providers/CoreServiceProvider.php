<?php


namespace App\Providers;

use App\Contract\Core\CommandBusInterface;
use App\Infrastructure\Core\CommandBus;
use Illuminate\Support\ServiceProvider;

/**
 * Class CoreServiceProvider
 * @package App\Providers
 */
class CoreServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(CommandBusInterface::class, CommandBus::class);
    }
}
