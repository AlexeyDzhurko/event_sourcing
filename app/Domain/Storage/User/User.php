<?php

namespace App\Domain\Storage\User;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

/**
 * Class User
 * @package App\Domain\User
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /** @var array $fillable */
    protected $fillable = [
        'name', 'email',
    ];

    /** @var array $casts */
    protected $casts = [
        'id' => 'string'
    ];

    /** @var array $hidden */
    protected $hidden = [
        'password',
    ];
}
