<?php

namespace App\Domain\Storage\User;

use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Interface RepositoryInterface
 * @package App\Domain
 */
interface UserRepositoryInterface
{
    /**
     * @param UserFilter $filter
     * @param PaginationInterface $paginator
     * @param SortingInterface|null $sorting
     * @return LengthAwarePaginator
     */
    public function allPagination(
        UserFilter $filter,
        PaginationInterface $paginator,
        ?SortingInterface $sorting
    ): LengthAwarePaginator;

    /**
     * @param UserFilter $filter
     * @param SortingInterface|null $sorting
     * @return Collection
     */
    public function all(
        UserFilter $filter,
        ?SortingInterface $sorting
    ): Collection;

    /**
     * @param UserFilter $filter
     * @return User|null
     */
    public function one(UserFilter $filter): ?Model;

    /**
     * @param string $id
     * @return Model|null
     */
    public function byId(string $id): ?Model;

    /**
     * @param User $model
     * @return User
     */
    public function store(User $model): User;

    /**
     * @param User $model
     */
    public function delete(User $model): void;
}
