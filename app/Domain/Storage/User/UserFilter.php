<?php

namespace App\Domain\Storage\User;

use Illuminate\Http\Request;

/**
 * Class UserFilter
 * @package App\Domain\User
 */
class UserFilter
{
    /** @var string $email */
    private $email;

    /**
     * @param Request $request
     * @return UserFilter
     */
    public static function fromRequest(Request $request)
    {
        return (new UserFilter())
            ->setEmail($request->get('email'));
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return UserFilter
     */
    public function setEmail(?string $email): UserFilter
    {
        $this->email = $email;

        return $this;
    }
}
