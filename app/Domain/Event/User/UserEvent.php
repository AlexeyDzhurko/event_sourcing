<?php

namespace App\Domain\Event\User;

use Jenssegers\Mongodb\Eloquent\Model;

/**
 * Class UserEvent
 * @package App\Domain\Event\User
 */
class UserEvent extends Model
{
    const USER_EVENT_CHANNEL = 'user.';
    const USER_EVENT_CREATE = 'create';
    const USER_EVENT_UPDATE = 'update';
    const USER_EVENT_DELETE = 'delete';

    /** @var string $connection */
    protected $connection = 'mongo';

    /** @var string $collection */
    protected $collection = 'users_event';

    /** @var array $fillable */
    protected $fillable = ['user_id', 'type', 'params'];
}
