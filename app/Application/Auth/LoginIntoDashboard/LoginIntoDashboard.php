<?php

namespace App\Application\Auth\LoginIntoDashboard;

use App\Contract\Core\Command;
use App\Infrastructure\Traits\AutofillTrait;

/**
 * Class LoginIntoDashboard
 * @package App\Application\Auth\LoginIntoDashboard
 */
class LoginIntoDashboard implements Command
{
    use AutofillTrait;

    /** @var string $email */
    private $email;

    /** @var string $password */
    private $password;

    /**
     * LoginIntoDashboard constructor.
     * @param array $formData
     */
    public function __construct(array $formData)
    {
        $this->autofillProperties($formData);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
