<?php

namespace App\Application\Auth\RefreshToken;

use App\Contract\Core\Command;

/**
 * Class RefreshToken
 * @package App\Application\Auth\RefreshToken
 */
class RefreshToken implements Command
{
    /** @var string $token */
    protected $token;

    /**
     * RefreshToken constructor.
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}
