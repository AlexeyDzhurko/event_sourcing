<?php

namespace App\Application\User\StoreUserEvent;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Event\User\UserEvent;

/**
 * Class StoreUserEventHandler
 * @package App\Application\User\StoreUserEvent
 */
class StoreUserEventHandler implements Handler
{
    /**
     * @param StoreUserEvent|Command $command
     * @return mixed|void
     */
    public function handle(Command $command)
    {
        $event = new UserEvent();
        $event->user_id = $command->getId();
        $event->type = $command->getType();
        $event->params = $command->getParams();

        $event->save();

        return $event;
    }

}
