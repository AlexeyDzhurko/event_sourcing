<?php

namespace App\Application\User\StoreUserEvent;

use App\Contract\Core\Command;
use Illuminate\Support\Collection;

/**
 * Class StoreUserEvent
 * @package App\Application\User\StoreUserEvent
 */
class StoreUserEvent implements Command
{
    /** @var array $params */
    protected $params;

    /** @var string $type */
    protected $type;

    /** @var string|null */
    protected $id;

    /**
     * StoreUserEvent constructor.
     * @param array $params
     * @param string $type
     * @param string|null $id
     */
    public function __construct(
        string $type,
        ?array $params = null,
        ?string $id = null
    ) {
        $this->params = $params;
        $this->type = $type;
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getParams(): ?array
    {
        return $this->params;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id ?? uniqid();
    }
}
