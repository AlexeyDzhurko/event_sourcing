<?php

namespace App\Application\User\StoreUser;

use App\Contract\Core\Command;
use App\Infrastructure\Traits\AutofillTrait;

/**
 * Class StoreUser
 * @package App\Application\User\StoreUser
 */
class StoreUser implements Command
{
    use AutofillTrait;

    /** @var string $userId */
    protected $userId;

    /** @var array $params */
    protected $params;

    /**
     * StoreUser constructor.
     * @param string $userId
     * @param array $payload
     */
    public function __construct(string $userId, array $payload)
    {
        $this->userId = $userId;
        $this->params = $payload;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
