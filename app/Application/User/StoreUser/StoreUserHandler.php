<?php

namespace App\Application\User\StoreUser;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Storage\User\User;
use App\Domain\Storage\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

/**
 * Class StoreUserHandler
 * @package App\Application\User\StoreUser
 */
class StoreUserHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    protected $userRepository;

    /**
     * StoreUserHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param StoreUser|Command $command
     * @return void
     */
    public function handle(Command $command)
    {
        $user = new User();
        $params = $command->getParams();

        $user->id = $command->getUserId();
        $user->email = $params['email'];
        $user->name = $params['name'];
        $user->password = Hash::make($params['password']);

        $this->userRepository->store($user);
    }

}
