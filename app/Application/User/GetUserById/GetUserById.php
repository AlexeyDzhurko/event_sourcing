<?php

namespace App\Application\User\GetUserById;

use App\Contract\Core\Command;

/**
 * Class GetUserById
 * @package App\Application\User\GetUserById
 */
class GetUserById implements Command
{
    /** @var string $id */
    protected $id;

    /**
     * GetUserById constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
