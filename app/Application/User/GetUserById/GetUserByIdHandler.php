<?php

namespace App\Application\User\GetUserById;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Storage\User\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetUserByIdHandler
 * @package App\Application\User\GetUserById
 */
class GetUserByIdHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    protected $userRepository;

    /**
     * GetUserByIdHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetUserById|Command $command
     * @return Model|null
     */
    public function handle(Command $command)
    {
        return $this->userRepository->byId($command->getId());
    }
}
