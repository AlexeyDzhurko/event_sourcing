<?php

namespace App\Application\User\DeleteUser;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Storage\User\UserRepositoryInterface;

/**
 * Class DeleteUserHandler
 * @package App\Application\User\DeleteUser
 */
class DeleteUserHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    protected $userRepository;

    /**
     * DeleteUserHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param DeleteUser|Command $command
     * @return mixed|void
     */
    public function handle(Command $command)
    {
        $this->userRepository->delete($command->getUser());
    }
}
