<?php

namespace App\Application\User\DeleteUser;

use App\Contract\Core\Command;
use App\Domain\Storage\User\User;

/**
 * Class DeleteUser
 * @package App\Application\User\DeleteUser
 */
class DeleteUser implements Command
{
    /** @var User $user */
    protected $user;

    /**
     * DeleteUser constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
