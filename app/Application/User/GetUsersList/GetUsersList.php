<?php

namespace App\Application\User\GetUsersList;

use App\Contract\Core\Command;
use App\Domain\Storage\User\UserFilter;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;

/**
 * Class GetUsersList
 * @package App\Application\User\GetUsersList
 */
class GetUsersList implements Command
{
    /** @var UserFilter $filter */
    protected $filter;

    /** @var Pagination|null $pagination */
    protected $pagination;

    /** @var Sorting|null $sorting */
    protected $sorting;

    /**
     * GetUsersList constructor.
     * @param UserFilter $filter
     * @param Pagination|null $pagination
     * @param Sorting|null $sorting
     */
    public function __construct(
        UserFilter $filter,
        ?Pagination $pagination = null,
        ?Sorting $sorting = null
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return UserFilter
     */
    public function getFilter(): UserFilter
    {
        return $this->filter;
    }

    /**
     * @return Pagination|null
     */
    public function getPagination(): ?Pagination
    {
        return $this->pagination;
    }

    /**
     * @return Sorting|null
     */
    public function getSorting(): ?Sorting
    {
        return $this->sorting;
    }
}
