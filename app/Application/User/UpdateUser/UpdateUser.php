<?php

namespace App\Application\User\UpdateUser;

use App\Contract\Core\Command;
use App\Domain\Storage\User\User;
use App\Infrastructure\Traits\AutofillTrait;

/**
 * Class UpdateUser
 * @package App\Application\User\UpdateUser
 */
class UpdateUser implements Command
{
    use AutofillTrait;

    /** @var User $user */
    protected $user;

    /** @var array $params */
    protected $params;

    /**
     * UpdateUser constructor.
     * @param User $user
     * @param array $params
     */
    public function __construct(User $user, array $params)
    {
        $this->user = $user;
        $this->params = $params;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
