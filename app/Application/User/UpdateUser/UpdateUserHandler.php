<?php

namespace App\Application\User\UpdateUser;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Storage\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

/**
 * Class UpdateUserHandler
 * @package App\Application\User\UpdateUser
 */
class UpdateUserHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    protected $userRepository;

    /**
     * UpdateUserHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UpdateUser|Command $command
     * @return void
     */
    public function handle(Command $command)
    {
        $user = $command->getUser();
        $params = $command->getParams();

        if (!empty($params['email'])) {
            $user->email = $params['email'];
        }

        if (!empty($params['name'])) {
            $user->name = $params['name'];
        }

        if (!empty($params['password'])) {
            $user->password = Hash::make($params['password']);
        }

        $this->userRepository->store($user);
    }
}
