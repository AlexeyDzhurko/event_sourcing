## Introduction

This project was made for demonstrations CQRS (Event sourcing) project structure.
All parts of this repository work, and can be used as skeleton for new project or as separate parts.

#### This assembly includes:
- Web server: Nginx1.44
- NoSql event storage: MongoDb 3.6.16
- Sql data storage: Postgres 11
- Message (event) broker: Rabbitmq 3.7.5-management
- Php interpreter: Php 7.2
- Node js interpreter: Node 12

#### Code parts
- Api: php 7.2, Lumen 6
- Admin panel front part: js. Vue js (runing on Nuxt)

## Deploy

### Using makefile

- clone a project from the repository
- make sure your system supports makefile instructions. if not - please install needed packages
- copy `.env.example` to `.env` and set/change environment variables according to your instance
- use command `make` in your IDE console from project root directory
- change  directories permissions if need it (log, cache, e.t.c)

### Using `docker-compose` commands
- clone a project from the repository
- copy `.env.example` to `.env` and set/change environment variables according to your instance
- in project root directory use commands:
    * `docker-compose up -d --build`
    * `docker-compose exec php-fpm composer install`
    * `docker-compose exec node npm i` (admin panel front-end path)
    * `docker-compose exec node npm run dev` (admin panel front-end path)
- change  directories permissions if need it (log, cache, e.t.c)

After deploy you can work with project on ports, who you set up in `.env`

## Available makefile commands
* make (default) - deploy your instance according to environment (set up in `.env`)
* docker-build - run `docker-compose build` command
* docker-up - run `docker-compose up -d` command
* docker-rebuild - starts the chain of commands: 
    - `docker-compose down`
    - `docker-compose up -d`
    - `docker-compose exec php-fpm composer install`
* composer-install - run `docker-compose exec php-fpm composer install` command
* npm-install - run `docker-compose exec node npm i` command
* npm-run-dev - run `docker-compose exec node npm run dev` command

> You can change or add a command depending on your requirements.

## Documentations

#### General
* https://en.wikipedia.org/wiki/Command%E2%80%93query_separation (SQRS wiki)
* https://habr.com/ru/post/146429/

#### RabbitMq
* http://wiki.zeromq.org/whitepapers:amqp-analysis (AMQP wiki)
* https://www.rabbitmq.com/documentation.html (official doc)
* https://nuwber.github.io/rabbitevents/ (installed package)

#### MongoDb
* https://en.wikipedia.org/wiki/MongoDB (wiki)
* https://docs.mongodb.com/guides/ (guides)
