<?php

return [
    'default' => env('QUEUE_CONNECTION', 'rabbitmq'),
    'connections' => [
        'rabbitmq' => [
            'driver' => 'rabbitmq',
            'exchange' => env('RABBITMQ_EXCHENGE', 'events'),
            'host' => env('RABBITMQ_DEFAULT_HOST', 'rabbitmq'),
            'port' => env('RABBITMQ_DEFAULT_PORT', 5672),
            'user' => env('RABBITMQ_DEFAULT_USER', 'rabbitmq'),
            'pass' => env('RABBITMQ_DEFAULT_PASS', 'rabbitmq'),
            'vhost' => env('RABBITMQ_DEFAULT_VHOST', '/'),
            'logging' => [
                'enabled' => env('RABBITEVENTS_LOG_ENABLED', false),
                'level' => env('RABBITEVENTS_LOG_LEVEL', 'info'),
            ]
        ],
    ],
];
